package starter;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {
                "pretty","json:target/cucumber/report.json",
                "html:target/cucumber/report.html"},
        features = "src/test/resources/features"
)
public class TestRunner {}
