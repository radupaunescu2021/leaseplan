package starter.stepdefinitions;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import starter.util.TestConstants;

public class CarsAPI {

    public Response getCar() {
        return SerenityRest
                .given()
                .contentType(ContentType.JSON)
                .baseUri(TestConstants.BASE_URL)
                .when()
                .get("car");
    }
}
