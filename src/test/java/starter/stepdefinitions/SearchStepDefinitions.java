package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import starter.util.TestConstants;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.*;


public class SearchStepDefinitions {

    @Steps
    public CarsAPI carsAPI;

    private Response response;

    @When("he calls endpoint {string}")
    public void heCallsEndpoint(String endpoint) {
        SerenityRest
                .given()
                .contentType(ContentType.JSON)
                .baseUri(TestConstants.BASE_URL)
                .when()
                .get(endpoint);
    }

    @Then("he doesn not see the results")
    public void he_Doesn_Not_See_The_Results() {
        restAssuredThat(response -> response.statusCode(404));
        restAssuredThat(response -> response.body("detail.error", equalTo(true)));
    }

    @Then("he sees the results displayed for {string}")
    public void heSeesTheResultsDisplayedForProduct(String productName) {
        restAssuredThat(response -> response.statusCode(200)
            .body("title", hasItem(containsString(productName))));
    }

    @When("he calls car endpoint")
    public void heCallsEndpointToGetCar() {
        carsAPI.getCar();
    }

}
