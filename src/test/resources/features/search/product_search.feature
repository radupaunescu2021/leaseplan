Feature: Search for the product

  Scenario: Positive scenario - Search for an available product (Orange)
    When he calls endpoint "orange"
    Then he sees the results displayed for "Orange"

  Scenario: Positive scenario - Search for an available product (apple)
    When he calls endpoint "apple"
    Then he sees the results displayed for "apple"


  Scenario: Positive scenario - Search for an available product (pasta)
    When he calls endpoint "pasta"
    Then he sees the results displayed for "pasta"


  Scenario: Positive scenario - Search for an available product (cola)
    When he calls endpoint "cola"
    Then he sees the results displayed for "cola"


  Scenario: Negative scenario - Search for a non-existent product
    When he calls endpoint "non_existent_product"
    Then he doesn not see the results


  Scenario: Negative scenario - Search for a product with special characters
    When he calls endpoint "!@#$%"
    Then he doesn not see the results


  Scenario: Negative scenario - Search for an available product (car)
    When he calls car endpoint
    Then he doesn not see the results
