# Cucumber Serenity GitLab CI/CD Project

## Installation

1. Clone the repository
2. Make sure you have Maven and JDK 8 installed on your system.
3. Run `mvn clean compile` to build the project.

## Running Tests

Run `mvn verify` to execute the tests.
HTML Reports are available at target/site/serenity/index.html


## Writing New Tests
1. Everytime a change is pushed on GitLab the pipeline starts and runs a build and test job
2. Add/modify tests
3. Run `mvn verify` to check test run correctly
4. Commit and push changes 


## CI/CD

1. Everytime a new changes is pushed on master branch a CI/CD pipeline starts
2. Serenity html report is also available on Gitlab,by accessing `Deploy` job,click on Browse-->Public-->Serenity--->index.html

## What was refactored
1. Renamed feature file 
2. Created `TestConstants` file to hold `BASE_URL`
3. Added new positive and negative scenarios to the `product_search.feature` file.
4. Implemented corresponding step definitions in the `SearchStepDefinitions` class.
5. Created a call to car api in the `CarAPI` class
6. Created .gitlab-ci.yml file to enable CI/CD on Gitlab
7. Removed gradle config files
8. Configured Html reporting in `serenitiy.conf` file

